package com.example.healthwear;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Register Activity";
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private User mUser;
    private Button buttonRegister;
    private EditText editTextEmail, editTextPassword;
    private TextView textViewSignIn;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        //Defining fields
        buttonRegister = findViewById(R.id.buttonRegister);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        textViewSignIn = findViewById(R.id.textViewSignin);
        progressDialog = new ProgressDialog(this);

        buttonRegister.setOnClickListener(this);
        textViewSignIn.setOnClickListener(this);
    }

    private void registerUser() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)) {
            //email is empty
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            //Don't execute further
            return;
        }

        if(TextUtils.isEmpty(password)) {
            //password is empty
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        //If validations are ok
        //Show a progress bar

        //Create user in authentication database and update public user class
        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            progressDialog.setMessage("Registering user...");
                            progressDialog.show();

                            Log.w(TAG, "Account created");
                            //Create user object
                            mUser = new User();
                            mUser.setUserID(mAuth.getCurrentUser().getUid());
                            mUser.setEmail(mAuth.getCurrentUser().getEmail());
                            //Add user to database with generic info
                            db.collection("users").document(mUser.getUserID())
                                    .set(mUser)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "Success adding user data");
                                            finish();
                                            startActivity(new Intent(getApplicationContext(), TabActivity.class));
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Fail adding user data", e);
                                        }
                                    });

                        }
                        else {
                            Log.d(TAG, "Account creation failed", task.getException());
                            Toast.makeText(RegisterActivity.this, "Could not register... Please try again : " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        progressDialog.dismiss();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            //Proceed TODO: If user contains no user database, send to profile tab with popup (Please submit data)
            finish();
            startActivity(new Intent(this, TabActivity.class));
        }
    }

    @Override
    public void onClick(View view) {
        if(view == buttonRegister) {
            //Register
            registerUser();
        }

        if(view == textViewSignIn) {
            //Switch to sign in activity
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}
