package com.example.healthwear;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.HashMap;

@IgnoreExtraProperties
public class User {
    private String userID;
    private String email;
    private int activityScore = 0; //Abstraction of fitness
    private String userData; //User dataset
    private int goalsCompleted; //Progress made towards goal milestone

    private @ServerTimestamp Date timeStamp;

    //Public constructor
    public User(String userID, String email, int activityScore) {
        this.userID = userID;
        this.email = email;
        this.activityScore = activityScore;
    }

    //Needed for FireStore conversion
    public User() { }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getActivityScore() {
        return activityScore;
    }

    public void setActivityScore(int activityScore) {
        this.activityScore = activityScore;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public String getUserData() {
        return userData;
    }

    public void setUserData(String userData) {
        this.userData = userData;
    }

    public int getGoalsCompleted() {
        return goalsCompleted;
    }

    public void setGoalsCompleted(int goalsCompleted) {
        this.goalsCompleted = goalsCompleted;
    }
}
