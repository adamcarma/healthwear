package com.example.healthwear;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

public class ProfileFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = "Profile fragment";

    private Button btnLogout, btnSendData;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DataSet dataSet = new DataSet();
    private User mUser;
    private Random rng;
    private WriteBatch batch;
    private Calendar calendar;
    private RecyclerView summaryRecycler;
    private RecyclerView.Adapter recyclerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<UserGoal> listOfGoals;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        batch = db.batch();
        rng = new Random();
        mUser = new User();
        if (mAuth.getCurrentUser() != null) {
            mUser.setUserID(mAuth.getCurrentUser().getUid());
        }
        else {
            startActivity(new Intent(getActivity(), RegisterActivity.class));
            getActivity().finish();
        }


        calendar = Calendar.getInstance();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);

        //Create recycler view
        summaryRecycler = view.findViewById(R.id.summaryRecycler);
        layoutManager = new LinearLayoutManager(getContext());
        summaryRecycler.setLayoutManager(layoutManager);

        //Display recycler view
        showGoalSummary();

        //TODO: Check for user database and popup message if required.
        db.collection("users").document(mAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.get("userData") != null) {
                            Log.w(TAG, "User database exists online.");
                            //Therefore do nothing //TODO: Remember to add option to delete data
                        }
                        else {
                            Log.w(TAG, "User database is not stored/does not exist. Sending popup");
                            //Ask user to provide data
                            sendNotification();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error getting user database field", e);
                    }
                });


        btnLogout = view.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Logged out", Toast.LENGTH_SHORT).show();
                mAuth.signOut();
                startActivity(new Intent(getActivity(), RegisterActivity.class));
                getActivity().finish();
            }
        });

        btnSendData = view.findViewById(R.id.btnSendData);
        btnSendData.setOnClickListener(this);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    //Sends user dataset to the database, records dataset id and stores it in user class, updating database
    private void sendData() {
        //Update user info with dataset
        db.collection("users").document(mUser.getUserID())
                .update("userData", dataSet.getDataID())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Success adding user data ID to user");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Fail adding user data ID to user", e);
                    }
                });

        //Add dataset to database
        db.collection("datasets").document(dataSet.getDataID())
                .set(dataSet)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Success adding dataset");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failure adding dataset", e);
                    }
                });

        //TODO:Start dataset from This date
        calendar.set(Calendar.DAY_OF_WEEK, 7); //Set to one day before current week for debugging purposes
        calendar.set(Calendar.WEEK_OF_YEAR, calendar.get(Calendar.WEEK_OF_YEAR)-1); //TODO: Current dataset is centered on 22 april

        dataSet.setDays(9);
        //Simulate the program populating a dataset class and sending information to database
        for (int i = 0; i < dataSet.getDaysContained(); ++i) { //For each day in the dataset, create day object
            if(i == 1) {
                calendar.set(Calendar.WEEK_OF_YEAR, calendar.get(Calendar.WEEK_OF_YEAR)+1);
                calendar.set(Calendar.DAY_OF_WEEK, 1);
            }
            if(i == 7) {
                calendar.set(Calendar.WEEK_OF_YEAR, calendar.get(Calendar.WEEK_OF_YEAR)+1);
                calendar.set(Calendar.DAY_OF_WEEK, 1);
            }

            DataSetDay day = new DataSetDay(); //Day 1->total days
            day.setDayIndex(i);

            float rngFloat = rng.nextFloat()*5; //Value between 0 and 5
            day.setDistanceWalked(rngFloat); //Set distance walked between 0km and 5km
            rngFloat *= 1400; //Average steps per km
            day.setStepsTaken((int) rngFloat); //Steps taken
            rngFloat = rng.nextFloat()*5; //Random time (hours) stood
            day.setTimeStood(rngFloat); //Set time stood

            day.setDate(formatCurrentDate(calendar.getTime())); //Set day Date to generated date
            calendar.set(Calendar.DAY_OF_WEEK, i+1);

            DocumentReference ref = db.collection("datasets").document(dataSet.getDataID()).collection("days").document();
            batch.set(ref, day);
        }
        batch.commit().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.w(TAG, "Batch complete");
            }
        });
    }

    private String formatCurrentDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM'T'HH:mm"); //Format date
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); //Time zone
        return dateFormat.format(date);
    }

    //TODO: Add ability to add progression, more days to the dataset and check for goal completion
    //Add user's goals to recycler view to display summary of weekly goals completed to the user
    private void showGoalSummary() {
        //Enter user goals section in database
        db.collection("users").document(mUser.getUserID()).collection("goals")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        Log.w(TAG, "Successfully reached users' goals collection");
                        listOfGoals = new ArrayList<>();
                        listOfGoals.clear();
                        for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {

                            if (doc.get("name") != null) {
                                String docName = doc.get("name").toString();

                                switch (docName) {
                                    case "Goals goal":
                                        Log.w(TAG, "Goals goal ignored");

                                        break;
                                    case "Steps goal":
                                        Log.w(TAG, "Steps goal ignored");

                                        break;
                                    case "Stood goal":
                                        Log.w(TAG, "Stood goal ignored");

                                        break;
                                    //Add all remaining goals (which must be user defined) to array
                                    default:
                                        if (doc.get("completionTime") != null) {
                                            listOfGoals.add(doc.toObject(UserGoal.class));
                                        }
                                        break;
                                }

                            }
                        }
                        if (listOfGoals.size() >= 1) {
                            Log.w(TAG, "Obtained user goals, creating recycler view.");
                            //Set up adapter with custom list
                            recyclerAdapter = new RecyclerAdapter(getContext(), listOfGoals);
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(summaryRecycler.getContext(),
                                    DividerItemDecoration.VERTICAL);
                            //Aesthetics
                            summaryRecycler.addItemDecoration(dividerItemDecoration);
                            //Attach adapter
                            summaryRecycler.setAdapter(recyclerAdapter);
                        }
                        else {
                            Log.w(TAG, "No user goals exist, creating empty recycler");
                            //Create empty goal that can be displayed within the adapter to notify user that no completed goals exist yet
                            UserGoal emptyGoal = new UserGoal();
                            emptyGoal.setName("No completed goals exist yet!");
                            listOfGoals.add(emptyGoal);

                            //Attach adapter
                            recyclerAdapter = new RecyclerAdapter(getContext(), listOfGoals);
                            summaryRecycler.setAdapter(recyclerAdapter);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "No user goals collection exist");
                    }
                });
    }

    //Sends user data notification
    private void sendNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.getContext(), "Notifications")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("No dataset exists for your account yet")
                .setContentText("Please click on submit dataset on the profile tab.")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.getContext());

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(2, builder.build());
        Log.w(TAG, "Sent notification");
    }


    @Override
    public void onClick(View view) {
        if (view == btnSendData) {
            sendData(); //TODO: Should delete current data if existing
            Toast.makeText(getActivity(), "Dataset submitted to server!", Toast.LENGTH_SHORT).show();
        }
    }
}
