package com.example.healthwear;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    //Goal strings
    private List<UserGoal> mGoals;
    //Used to display view
    private LayoutInflater mInflater;

    RecyclerAdapter(Context context, List<UserGoal> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mGoals = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserGoal mGoal = mGoals.get(position);
        holder.rowGoalName.setText(mGoal.getName());
        //If statements are needed to allow empty goal with only goal name
        if (mGoal.getTimeStamp() != null) {
            holder.rowGoalStartTime.setText(" Set: " + formatCurrentDate(mGoal.getTimeStamp()));
        }
        if (mGoal.getCompletionTime() != null) {
            holder.rowGoalEndTime.setText(" Achieved: " + formatCurrentDate(mGoal.getCompletionTime()));
        }
    }

    private String formatCurrentDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM' 'HH:mm"); //Format date
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); //Time zone
        return dateFormat.format(date);
    }

    @Override
    public int getItemCount() {
        return mGoals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView rowGoalName, rowGoalStartTime, rowGoalEndTime;

        ViewHolder(View itemView) {
            super(itemView);
            rowGoalName = itemView.findViewById(R.id.rowGoalName);
            rowGoalStartTime = itemView.findViewById(R.id.rowGoalStartTime);
            rowGoalEndTime = itemView.findViewById(R.id.rowGoalEndTime);
            itemView.setOnClickListener(this);
        }

        //No onClick needed for this tab
        @Override
        public void onClick(View view) {
        }
    }
}
