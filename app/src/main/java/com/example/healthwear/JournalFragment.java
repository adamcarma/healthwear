package com.example.healthwear;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class JournalFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "Journal fragment";

    private Button btnTEST, buttonCompleteGoal, buttonDeleteGoal, buttonUndoCompletion;
    private String stepsGoal, timeStoodGoal, goalsTarget, userDefinedGoalText;
    private EditText stepsGoalEditText, timeStoodGoalEditText, goalsGoalEditText, userDefinedGoalsEditText;
    private TextView textTitle3;
    private int stepsGoalVal = -1;
    private int goalsGoalVal;
    private float timeStoodGoalVal;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private User mUser;
    private Spinner userDefinedGoalsSpinner;
    private List<UserGoal> listOfUserGoals;

    //Define empty temp goal for buttons to modify and use
    private UserGoal tempGoal;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = new User();
        mUser.setUserID(mAuth.getCurrentUser().getUid());
        tempGoal = new UserGoal();

        stepsGoal = "";
        timeStoodGoal = "";
        goalsTarget = "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.journal_fragment, container, false);

        stepsGoalEditText = view.findViewById(R.id.stepsGoal);
        timeStoodGoalEditText = view.findViewById(R.id.timeStoodGoal);
        goalsGoalEditText = view.findViewById(R.id.goalsGoal);
        textTitle3 = view.findViewById(R.id.textTitle3);
        userDefinedGoalsEditText = view.findViewById(R.id.userDefinedGoal);
        userDefinedGoalsSpinner = view.findViewById(R.id.userDefinedGoalsSpinner);
        userDefinedGoalsSpinner.setOnItemSelectedListener(this);

        btnTEST = view.findViewById(R.id.btnTEST2);
        buttonCompleteGoal = view.findViewById(R.id.btnCompleteGoal);
        buttonDeleteGoal = view.findViewById(R.id.btnDeleteGoal);
        buttonUndoCompletion = view.findViewById(R.id.btnUndoCompletion);

        btnTEST.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Saved goals", Toast.LENGTH_SHORT).show();
                sendUserInput();
            }
        });

        buttonCompleteGoal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Completed goal", Toast.LENGTH_SHORT).show();
                completeGoal();
            }
        });

        buttonDeleteGoal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Deleted goal", Toast.LENGTH_SHORT).show();
                deleteGoal(tempGoal);
            }
        });

        buttonUndoCompletion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Undid completion", Toast.LENGTH_SHORT).show();
                undoCompletion();
            }
        });

        getGoalsData();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    private void sendUserInput() {
        //When button is pressed, any modified EditTexts will have their user input saved to the database
        if (stepsGoalEditText.length() != 0) {
            stepsGoalVal = Integer.parseInt(stepsGoalEditText.getText().toString());
            UserGoal stepGoal = new UserGoal("Steps goal", stepsGoalVal);
            db.collection("users").document(mUser.getUserID()).collection("goals").document(stepGoal.getName())
                    .set(stepGoal)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            stepsGoal = stepsGoalEditText.getText().toString();
                            Log.w(TAG, "Added a steps goal to current user");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "Failed to add steps goal to current user", e);
                        }
                    });
        }
        if (timeStoodGoalEditText.length() != 0) {
            //Record this data
            timeStoodGoalVal = Float.parseFloat(timeStoodGoalEditText.getText().toString());
            UserGoal stoodGoal = new UserGoal("Stood goal", timeStoodGoalVal);
            db.collection("users").document(mUser.getUserID()).collection("goals").document(stoodGoal.getName())
                    .set(stoodGoal)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            timeStoodGoal = timeStoodGoalEditText.getText().toString();
                            Log.w(TAG, "Added a time stood goal to current user");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "Failed to add time stood goal to current user", e);
                        }
                    });

        }
        if (goalsGoalEditText.length() != 0) {
            //Record this data
            goalsGoalVal = Integer.parseInt(goalsGoalEditText.getText().toString());
            UserGoal goalsGoal = new UserGoal("Goals goal", goalsGoalVal);
            db.collection("users").document(mUser.getUserID()).collection("goals").document(goalsGoal.getName())
                    .set(goalsGoal)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            goalsTarget = goalsGoalEditText.getText().toString();
                            Log.w(TAG, "Added a weekly goals goal to current user");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "Failed to add weekly goals goal to current user", e);
                        }
                    });

        }
        if (userDefinedGoalsEditText.length() != 0) {
            //Record this data
            userDefinedGoalText = userDefinedGoalsEditText.getText().toString();

            //In this case, value will be to determine whether the goal has been completed or not
            final UserGoal userDefinedGoal = new UserGoal();
            userDefinedGoal.setName(userDefinedGoalText);
            db.collection("users").document(mUser.getUserID()).collection("goals").document(userDefinedGoal.getName())
                    .set(userDefinedGoal)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.w(TAG, "Added a user defined goal to current user");
                            //This is needed to update the spinner (Could also call getGoalsData again)
                            //Note: Unintended behaviour if they enter keyword
                            listOfUserGoals.add(userDefinedGoal);
                            updateSpinner();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e(TAG, "Failed to add user defined goal to current user", e);
                        }
                    });

        }
    }
    //Run every time view is created to populate class fields and provide notifications with required info
    private void getGoalsData() {

        db.collection("users").document(mUser.getUserID()).collection("goals")
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        Log.w(TAG, "Successfully reached users' goals collection");
                        listOfUserGoals = new ArrayList<>();
                        listOfUserGoals.clear();
                        for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {

                            if (doc.get("name") != null) {
                                String docName = doc.get("name").toString();

                                switch (docName) {
                                    case "Goals goal":
                                        goalsTarget = docName;
                                        Log.w(TAG, "Goals goal stored");

                                        break;
                                    case "Steps goal":
                                        stepsGoal = docName;
                                        Log.w(TAG, "Steps goal stored");

                                        break;
                                    case "Stood goal":
                                        timeStoodGoal = docName;
                                        Log.w(TAG, "Stood goal stored");

                                        break;
                                    //Add all remaining goals (which must be user defined) to array
                                    default:
                                        listOfUserGoals.add(doc.toObject(UserGoal.class));
                                        break;
                                }

                            }
                        }
                        sendNotification(); //Once having stored goal vals, check if notification should be sent
                        //Now update spinner with listOfUserGoals strings
                        updateSpinner();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "No user goals collection exist");
                        sendNotification();
                    }
                });

    }

    private void updateSpinner() {
        //We want a list of strings (All user goals)
        ArrayList<String> listOfUserGoalNames = new ArrayList<>();
        //For each goal object
        for (int i = 0; i < listOfUserGoals.size(); ++i) {
            //Extract string 'goal name' and store in string array
            listOfUserGoalNames.add(listOfUserGoals.get(i).getName());
            Log.w(TAG, "GOAL NAME : " + listOfUserGoalNames.get(i));
        }
        if (!listOfUserGoals.isEmpty()) {
            ArrayAdapter adapter = new ArrayAdapter(this.getContext(), android.R.layout.simple_spinner_item, listOfUserGoalNames);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            userDefinedGoalsSpinner.setAdapter(adapter);
            userDefinedGoalsSpinner.setVisibility(View.VISIBLE);
            textTitle3.setVisibility(View.VISIBLE);
            Log.w(TAG, "List of user goals is not empty. Setting spinner with values and setting visibility");
        }
        else {
            userDefinedGoalsSpinner.setVisibility(View.INVISIBLE);
            textTitle3.setVisibility(View.INVISIBLE);
            buttonCompleteGoal.setVisibility(View.INVISIBLE);
            buttonDeleteGoal.setVisibility(View.INVISIBLE);
            buttonUndoCompletion.setVisibility(View.INVISIBLE);
        }
    }

    private void sendNotification() {
        //If any recommended goals do not exist, notify user that they should set them.
        if (stepsGoal.isEmpty() || timeStoodGoal.isEmpty() || goalsTarget.isEmpty()) { //If at least one goal has not been set yet
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this.getContext(), "Notifications")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Recommended goals not set!")
                    .setContentText("Please enter some recommended goals.")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.getContext());

            // notificationId is a unique int for each notification that you must define
            notificationManager.notify(1, builder.build());
            Log.w(TAG, "Sent notification");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String item = parent.getItemAtPosition(position).toString();
        for (UserGoal goal : listOfUserGoals) {
            if (goal.getName().equals(item)) {
                tempGoal = goal;
                updateUI(tempGoal);
                break;
            }
        }
    }
    //When an item in the spinner is selected, we would like to have basic information
    //such as 'isCompleted' and option to complete goals to show up.
    private void updateUI(UserGoal goal) {
        buttonUndoCompletion.setVisibility(View.INVISIBLE);
        buttonCompleteGoal.setVisibility(View.VISIBLE);

        buttonDeleteGoal.setVisibility(View.VISIBLE);
        Log.w(TAG, goal.getName());

        if (goal.isComplete()) {
            Log.w(TAG, "Button set invisible!");
            //Set button to undo completion
            buttonCompleteGoal.setVisibility(View.INVISIBLE);
            buttonUndoCompletion.setVisibility(View.VISIBLE);
        }

    }

    private void completeGoal() {
        //Contact server and update completion field of current goal to true
        db.collection("users").document(mUser.getUserID()).collection("goals").document(tempGoal.getName())
                .update("complete", true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        tempGoal.setComplete(true);
                        Log.w(TAG, "Completed selected goal");

                        updateUI(tempGoal);
                    }
                });

        db.collection("users").document(mUser.getUserID()).collection("goals").document(tempGoal.getName())
                .update("completionTime", getCurrentTimeStamp())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Updated completion time");
                    }
                });

        //Iterate current user's goals completed value on database
        db.collection("users").document(mUser.getUserID())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        User curUser = documentSnapshot.toObject(User.class);

                        //Now iterate goals completed
                        db.collection("users").document(mUser.getUserID())
                                .update("goalsCompleted", curUser.getGoalsCompleted()+1)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.w(TAG, "Successfully incremented goals completed");
                                    }
                                });
                    }
                });
    }

    //Used to update goal completion date.
    private Date getCurrentTimeStamp() {
        //Get current day instance
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    private void undoCompletion() {
        //Contact server and undo completion field of selected goal
        db.collection("users").document(mUser.getUserID()).collection("goals").document(tempGoal.getName())
                .update("complete", false)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Undone completion of selected goal");
                        tempGoal.setComplete(false);
                        updateUI(tempGoal);

                        db.collection("users").document(mUser.getUserID()).collection("goals").document(tempGoal.getName())
                                .update("completionTime", null)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.w(TAG, "Removed completion time");
                                    }
                                });
                    }
                });

        //Decrement current user's goals completed value on database
        db.collection("users").document(mUser.getUserID())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        User curUser = documentSnapshot.toObject(User.class);

                        //Now iterate goals completed
                        db.collection("users").document(mUser.getUserID())
                                .update("goalsCompleted", curUser.getGoalsCompleted()-1)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.w(TAG, "Successfully decremented goals completed");
                                    }
                                });
                    }
                });
    }

    private void deleteGoal(final UserGoal goal) {
        db.collection("users").document(mUser.getUserID()).collection("goals").document(goal.getName())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Deleted selected goal");
                        listOfUserGoals.remove(goal);
                        updateSpinner();
                    }
                });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
