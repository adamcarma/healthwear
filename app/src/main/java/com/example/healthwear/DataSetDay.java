package com.example.healthwear;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class DataSetDay {
    private int dayIndex;
    private int stepsTaken = 0;
    private float distanceWalked = 0.0F;
    private float timeStood = 0.0F;
    private String date;

    //Needed for FireStore conversion
    public DataSetDay() { }

    public int getStepsTaken() {
        return stepsTaken;
    }

    public void setStepsTaken(int stepsTaken) {
        this.stepsTaken = stepsTaken;
    }

    public float getDistanceWalked() {
        return distanceWalked;
    }

    public void setDistanceWalked(float distanceWalked) {
        this.distanceWalked = distanceWalked;
    }

    public int getDayIndex() {
        return dayIndex;
    }

    public void setDayIndex(int dayIndex) {
        this.dayIndex = dayIndex;
    }

    public float getTimeStood() {
        return timeStood;
    }

    public void setTimeStood(float timeStood) {
        this.timeStood = timeStood;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
