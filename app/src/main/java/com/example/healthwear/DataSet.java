package com.example.healthwear;

import android.util.Log;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;
import java.util.UUID;

@IgnoreExtraProperties
public class DataSet {
    private String dataID; //Will be also stored in user database to link to individuals
    private int daysContained = 14; //Number of days stored within this set (Simulated for now)
    private @ServerTimestamp Date timeStamp; //Time updated in Firestore

    //Public Constructor
    public DataSet() {
        dataID = UUID.randomUUID().toString();
        Log.w("Dataset class", "Data ID generated");
    }

    public String getDataID() {
        return dataID;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public int getDaysContained() {
        return daysContained;
    }

    public void setDays(int days) {
        this.daysContained = days;
    }
}
