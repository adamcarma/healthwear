package com.example.healthwear;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class SocialFragment extends Fragment {
    private static final String TAG = "Social fragment";
    //Icons made by "https://www.flaticon.com/authors/freepik" from "https://www.flaticon.com"

    private FirebaseFirestore db;
    private List<User> mUsers;
    private TextView textView0Column0, textView0Column1, textView1Column0, textView1Column1, textView2Column0, textView2Column1,
            textView3Column0, textView3Column1, textView4Column0, textView4Column1, textView5Column0, textView5Column1,
            textView6Column0, textView6Column1, textView7Column0, textView7Column1, textView8Column0, textView8Column1,
            textView9Column0, textView9Column1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.social_fragment, container, false);

        //Leaderboard
        textView0Column0 = view.findViewById(R.id.textView0Column0);
        textView0Column1 = view.findViewById(R.id.textView0Column1);
        textView1Column0 = view.findViewById(R.id.textView1Column0);
        textView1Column1 = view.findViewById(R.id.textView1Column1);
        textView2Column0 = view.findViewById(R.id.textView2Column0);
        textView2Column1 = view.findViewById(R.id.textView2Column1);
        textView3Column0 = view.findViewById(R.id.textView3Column0);
        textView3Column1 = view.findViewById(R.id.textView3Column1);
        textView4Column0 = view.findViewById(R.id.textView4Column0);
        textView4Column1 = view.findViewById(R.id.textView4Column1);
        textView5Column0 = view.findViewById(R.id.textView5Column0);
        textView5Column1 = view.findViewById(R.id.textView5Column1);
        textView6Column0 = view.findViewById(R.id.textView6Column0);
        textView6Column1 = view.findViewById(R.id.textView6Column1);
        textView7Column0 = view.findViewById(R.id.textView7Column0);
        textView7Column1 = view.findViewById(R.id.textView7Column1);
        textView8Column0 = view.findViewById(R.id.textView8Column0);
        textView8Column1 = view.findViewById(R.id.textView8Column1);
        textView9Column0 = view.findViewById(R.id.textView9Column0);
        textView9Column1 = view.findViewById(R.id.textView9Column1);

        getOrderedUsers();

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    //Send a database request that returns an ordered list of up to 10 users that have the highest goals on record
    private void getOrderedUsers() {
        db.collection("users")
                .orderBy("goalsCompleted", Query.Direction.DESCENDING)
                .limit(10)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        mUsers = queryDocumentSnapshots.toObjects(User.class);
                        Log.w(TAG, "Created a list of users of size: " + mUsers.size());
                        buildLeaderboard();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failed to get list of users");
                    }
                });
    }

    //Populate text views controlling for size of array
    private void buildLeaderboard() {

        if (mUsers.isEmpty()) {
            Log.e(TAG, "This shouldn't happen. No users exist in database.");
        }
        if (mUsers.size() >= 1) {
            //TextView 1 = mUser.get(1).getGoals;
            textView0Column0.setText(formatEmail(mUsers.get(0).getEmail())); //Name
            textView0Column1.setText(String.valueOf(mUsers.get(0).getGoalsCompleted())); //Goals completed
        }
        if (mUsers.size() >= 2) {
            textView1Column0.setText(formatEmail(mUsers.get(1).getEmail()));
            textView1Column1.setText(String.valueOf(mUsers.get(1).getGoalsCompleted()));
        }
        if (mUsers.size() >= 3) {
            textView2Column0.setText(formatEmail(mUsers.get(2).getEmail()));
            textView2Column1.setText(String.valueOf(mUsers.get(2).getGoalsCompleted()));
        }
        if (mUsers.size() >= 4) {
            textView3Column0.setText(formatEmail(mUsers.get(3).getEmail()));
            textView3Column1.setText(String.valueOf(mUsers.get(3).getGoalsCompleted()));
        }
        if (mUsers.size() >= 5) {
            textView4Column0.setText(formatEmail(mUsers.get(4).getEmail()));
            textView4Column1.setText(String.valueOf(mUsers.get(4).getGoalsCompleted()));
        }
        if (mUsers.size() >= 6) {
            textView5Column0.setText(formatEmail(mUsers.get(5).getEmail()));
            textView5Column1.setText(String.valueOf(mUsers.get(5).getGoalsCompleted()));
        }
        if (mUsers.size() >= 7) {
            textView6Column0.setText(formatEmail(mUsers.get(6).getEmail()));
            textView6Column1.setText(String.valueOf(mUsers.get(6).getGoalsCompleted()));
        }
        if (mUsers.size() >= 8) {
            textView7Column0.setText(formatEmail(mUsers.get(7).getEmail()));
            textView7Column1.setText(String.valueOf(mUsers.get(7).getGoalsCompleted()));
        }
        if (mUsers.size() >= 9) {
            textView8Column0.setText(formatEmail(mUsers.get(8).getEmail()));
            textView8Column1.setText(String.valueOf(mUsers.get(8).getGoalsCompleted()));
        }
        if (mUsers.size() >= 10) {
            textView9Column0.setText(formatEmail(mUsers.get(9).getEmail()));
            textView9Column1.setText(String.valueOf(mUsers.get(9).getGoalsCompleted()));
        }
    }

    private String formatEmail(String email) {
        return email.split("@")[0];
    }
}
