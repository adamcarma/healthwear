package com.example.healthwear;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

//Defines an instance of a user goal
@IgnoreExtraProperties
public class UserGoal {
    private String name;
    private float value = 0;
    private boolean complete = false;
    private @ServerTimestamp Date timeStamp;
    private Date completionTime;

    public UserGoal() {

    }

    public UserGoal(String name, float value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public void setComplete(boolean bool) {
        complete = bool;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public Date getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(Date completionTime) {
        this.completionTime = completionTime;
    }

    public boolean isComplete() {
        return complete;
    }
}
