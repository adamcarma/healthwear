package com.example.healthwear;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class HomeFragment extends Fragment{
    private static final String TAG = "Home fragment";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private User mUser;
    private ArrayList<DataSetDay> mUserDays; // List of days
    private DataSetDay dayShell; //Created to fill an array of size 7 with empty days to display on graph
    private ProgressBar pBarGoals, pBarSteps, pBarStood;
    private TextView textGoals, textSteps, textStood;
    private TextView textGoalsVal1, textGoalsVal2, textStepsVal1, textStepsVal2, textStoodVal1, textStoodVal2, textNoGoals;
    private String startOfWeek, endOfWeek;
    private BarChart chart;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        Log.w(TAG, getCurrentDate()); //Update startOfWeek, endOfWeek, currentDay

        //Check whether user is logged in
        if (mAuth.getCurrentUser() != null) {
            //Store user data
            mUser = new User();
            mUser.setUserID(mAuth.getCurrentUser().getUid());
            mUser.setEmail(mAuth.getCurrentUser().getEmail());
        }
        else {
            //Redirect to register activity
            startActivity(new Intent(getActivity(), RegisterActivity.class));
            getActivity().finish();
        }

        mUserDays = new ArrayList<>();
        for (int i = 0; i < 7; ++i) {
            dayShell = new DataSetDay();
            mUserDays.add(i, dayShell); //Initialise size 7 arrayList
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        //Here we set progress bars to invisible at load time. If a user has goals set, the associated progress bar(s) will show up
        pBarGoals = view.findViewById(R.id.progressBarGoals);
        pBarSteps = view.findViewById(R.id.progressBarSteps);
        pBarStood = view.findViewById(R.id.progressBarStanding);
        pBarGoals.setVisibility(View.INVISIBLE);
        pBarSteps.setVisibility(View.INVISIBLE);
        pBarStood.setVisibility(View.INVISIBLE);
        //Set text that does not change
        textGoals = view.findViewById(R.id.txtGoalProgress);
        textSteps = view.findViewById(R.id.txtStepsProgress);
        textStood = view.findViewById(R.id.txtStandingProgress);
        textNoGoals = view.findViewById(R.id.noGoalTextView);
        //Numerical values
        textGoalsVal1 = view.findViewById(R.id.txtGoalProgressValue1);
        textGoalsVal2 = view.findViewById(R.id.txtGoalProgressValue2);
        textStepsVal1 = view.findViewById(R.id.txtStepsProgressValue1);
        textStoodVal1 = view.findViewById(R.id.txtStandingProgressValue1);
        textStepsVal2 = view.findViewById(R.id.txtStepsProgressValue2);
        textStoodVal2 = view.findViewById(R.id.txtStandingProgressValue2);
        //Set all TextEdits to invisible
        textGoals.setVisibility(View.INVISIBLE);
        textSteps.setVisibility(View.INVISIBLE);
        textStood.setVisibility(View.INVISIBLE);
        textGoalsVal1.setVisibility(View.INVISIBLE);
        textStepsVal1.setVisibility(View.INVISIBLE);
        textStoodVal1.setVisibility(View.INVISIBLE);
        textGoalsVal2.setVisibility(View.INVISIBLE);
        textStepsVal2.setVisibility(View.INVISIBLE);
        textStoodVal2.setVisibility(View.INVISIBLE);

        //Graph connected to xml
        chart = view.findViewById(R.id.chart);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //Redirect new users to profile page if they have not submitted data
        doesUserDataExist();

        //Create graph
        getUserDays();

        //Show progress bars
        displayProgressBars();
    }

    private void doesUserDataExist() {
        db.collection("users").document(mAuth.getCurrentUser().getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.get("userData") != null) {
                            Log.w(TAG, "User database exists online.");
                            //Therefore do nothing
                        }
                        else {
                            Log.w(TAG, "User database is not stored/does not exist. Sending notification, Redirecting to Profile fragment");
                            //Redirect
                            TabLayout tabs = getActivity().findViewById(R.id.tabs);
                            tabs.getTabAt(3).select();

                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error getting user database field", e);
                    }
                });
    }

    private String getCurrentDate() {
        //Get current day instance
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy"); //Format date
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); //Time zone
        Date today = calendar.getTime(); //Function will return current day

        calendar.set(Calendar.DAY_OF_WEEK, 1); //Set instance to start of current week
        Date startOfWeekDate = calendar.getTime(); //Save this date
        calendar.set(Calendar.DAY_OF_WEEK, 7); //End of week
        Date endOfWeekDate = calendar.getTime(); //Save

        startOfWeek = dateFormat.format(startOfWeekDate);
        endOfWeek = dateFormat.format(endOfWeekDate);
        Log.w(TAG, startOfWeek);
        Log.w(TAG, endOfWeek);

        return dateFormat.format(today);
    }

    private void displayProgressBars() {
        db.collection("users").document(mUser.getUserID()).collection("goals").document("Steps goal")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.get("value") != null) {
                            Log.d(TAG, "Steps goal value is not null");
                            float tempValue = Float.parseFloat(documentSnapshot.get("value").toString());
                            //Set max for progress bar. Set progress bar to be visible.
                            pBarSteps.setVisibility(View.VISIBLE);
                            pBarSteps.setMax( (int) tempValue);
                            textSteps.setVisibility(View.VISIBLE);

                            textStepsVal2.setText(" / " + (int) tempValue);
                            textStepsVal2.setVisibility(View.VISIBLE);
                            textStepsVal1.setVisibility(View.VISIBLE); //If a goal exists, you need to show the progress too
                            Log.w(TAG, "Successfully set steps taken goal for progress bar");

                            LimitLine stepsGoalLine = new LimitLine(tempValue, "Your daily steps taken goal");
                            stepsGoalLine.setLineColor(Color.RED);
                            stepsGoalLine.setLineWidth(2f);
                            stepsGoalLine.setTextColor(Color.BLACK);
                            stepsGoalLine.setTextSize(12f);
                            chart.getAxisLeft().addLimitLine(stepsGoalLine);
                            chart.invalidate();
                        }
                        else Log.d(TAG, "Steps goal value is null");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failed to get steps goal from user/goals", e);
                    }
                });

        db.collection("users").document(mUser.getUserID()).collection("goals").document("Stood goal")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.get("value") != null) {
                            Log.d(TAG, "Stood goal value is not null");
                            float tempValue = Float.parseFloat(documentSnapshot.get("value").toString());
                            //Set max for progress bar. Set progress bar to be visible.
                            pBarStood.setVisibility(View.VISIBLE);
                            //This is needed to
                            tempValue*=100;
                            pBarStood.setMax( (int) tempValue);
                            textStood.setVisibility(View.VISIBLE);

                            tempValue/=100;
                            textStoodVal2.setText(" / " + (int) tempValue + " hours");
                            textStoodVal2.setVisibility(View.VISIBLE);
                            textStoodVal1.setVisibility(View.VISIBLE);
                            Log.w(TAG, "Successfully set time stood goal for progress bar");
                        }
                        else Log.d(TAG, "Time stood value is null");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failed to get time stood from user/goals", e);
                    }
                });

        db.collection("users").document(mUser.getUserID()).collection("goals").document("Goals goal")
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.get("value") != null) {
                            Log.d(TAG, "Goals goal value is not null");
                            float tempValue = Float.parseFloat(documentSnapshot.get("value").toString());
                            //Set max for progress bar. Set progress bar to be visible.
                            pBarGoals.setMax( (int) tempValue);
                            textGoals.setVisibility(View.VISIBLE);

                            textGoalsVal2.setText("" + (int) tempValue);
                            textGoalsVal2.setVisibility(View.VISIBLE);
                            textGoalsVal1.setVisibility(View.VISIBLE);
                            pBarGoals.setVisibility(View.VISIBLE);
                            Log.w(TAG, "Successfully set UserGoals goal for progress bar");
                        }
                        else {
                            Log.d(TAG, "User goals value is null");
                            textGoals.setVisibility(View.VISIBLE);
                            textNoGoals.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failed to get goals goal from user/goals", e);
                    }
                });

    }

    private void getUserDays() {


        //Get user's dataset ID
        db.collection("users").document(mUser.getUserID())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.get("userData") == null) {
                            Log.d(TAG, "User does not own a dataset");
                        }
                        else {
                            //Store if needed for another function
                            mUser.setUserData(documentSnapshot.getString("userData"));
                            Log.w(TAG, "Successfully stored 'userData' string in user object");

                            //Read days from current user's dataset and order by ascending day
                            db.collection("datasets").document(mUser.getUserData()).collection("days")
                                    .orderBy("dayIndex")
                                    .whereGreaterThanOrEqualTo("dayIndex", 1) //We start on start of week. (Since data includes 1 day before and 1 after)
                                    .limit(7)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if(task.isSuccessful()) {
                                                List<BarEntry> entries = new ArrayList<>(); //Entry array used to hold bar chart values for drawing

                                                int count = 0;
                                                for (QueryDocumentSnapshot doc : task.getResult()) {
                                                    mUserDays.set(count, doc.toObject(DataSetDay.class));
                                                    count++;
                                                }
                                                Log.w(TAG, "Successfully created ordered list of days");

                                                //Here we set progress textEdits
                                                buildStrings(mUserDays);

                                                for (DataSetDay days : mUserDays) { //For each day, store x value and steps taken (y value)
                                                    entries.add(new BarEntry(days.getDayIndex(), days.getStepsTaken()));
                                                }
                                                final String[] days = {"Mo", "Tu", "Wed", "Th", "Fr", "Sa", "Su"};

                                                BarDataSet bSet = new BarDataSet(entries, "Your daily steps taken this week");
                                                BarData bData = new BarData(bSet);
                                                bData.setBarWidth(0.9F);
                                                bData.setValueTextSize(10f);

                                                //Designing bar chart to look nice
                                                chart.getAxisRight().setEnabled(false);
                                                //chart.getAxisLeft().setEnabled(false);
                                                chart.getAxisLeft().setDrawAxisLine(false);
                                                chart.getAxisLeft().setDrawGridLines(false);
                                                chart.getAxisLeft().setDrawZeroLine(true);
                                                chart.getAxisLeft().setValueFormatter(new LargeValueFormatter());


                                                chart.setData(bData);
                                                chart.setFitBars(true);
                                                chart.getDescription().setEnabled(false);
                                                //Chart animation
                                                chart.animateXY(3000,3000, Easing.EaseInOutBounce);
                                                chart.getXAxis().setValueFormatter(new ValueFormatter() {
                                                    @Override
                                                    public String getAxisLabel(float value, AxisBase axis) {
                                                        return days[(int) value-1]; //Turn x values from index to human readable
                                                    }
                                                });

                                                chart.invalidate(); //Refresh

                                            } else {
                                                Log.d(TAG, "Error getting days in order and adding to array", task.getException());
                                            }
                                        }
                                    });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failed to get user's dataset ID", e);
                    }
                });
    }

    //Works out current weekly progress
    private void buildStrings(ArrayList<DataSetDay> datasetDays) {
        int currentSteps, currentStood;

        if (datasetDays.size()>0) { //If days within dataset exist
            currentSteps = datasetDays.get(datasetDays.size()-1).getStepsTaken();
            currentStood = (int) datasetDays.get(datasetDays.size()-1).getTimeStood();

            textStepsVal1.setText(String.valueOf(currentSteps));
            pBarSteps.setProgress(currentSteps);
            Log.w(TAG, "Current steps = " + currentSteps);

            textStoodVal1.setText(String.valueOf(currentStood));
            pBarStood.setProgress(currentStood*100);
            Log.w(TAG, "Current stood = " + currentStood);
        }
        else Log.e(TAG, "Cannot build strings when there is no dataset");

        getGoalProgress(); //Now build string for goal progress

    }

    private void getGoalProgress() {
        db.collection("users").document(mUser.getUserID())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.get("goalsCompleted") != null) {

                            float tempValue = Float.parseFloat(documentSnapshot.get("goalsCompleted").toString());
                            pBarGoals.setProgress( (int) tempValue);
                            Log.w(TAG, "Successfully set goals progress for progress bar" + (int) tempValue);

                            textGoalsVal1.setText(documentSnapshot.get("goalsCompleted").toString() + " / ");
                        }
                        else {
                            Log.w(TAG, "Completed goals for current user is null");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Failed to get goals completed for current user", e);
                    }
                });
    }
}
